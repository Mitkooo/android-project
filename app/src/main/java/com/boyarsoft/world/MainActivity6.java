package com.boyarsoft.world;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity6 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        TextView tv;

        tv = (TextView) findViewById(R.id.textView9);

        tv.setText("Австралийският съюз е шестата по големина държава в света и единствената, която заема цял континент – Австралия. Тя е разположена в южното полукълбо и е " +
                "най-голямата страна в Австралазия. Австралия включва остров Тасмания, който е австралийски щат и множество по-малки острови в Индийския и Тихия океан. Съседните " +
                "страни на Австралия са Нова Зеландия на югоизток и Индонезия, Папуа Нова Гвинея и Източен Тимор на север и Соломонови острови, Вануату и Нова Каледония на " +
                "североизток. Името „Австралия“ идва от латинската фраза terra australis incognita (тера аустралис инкогнита), което значи непозната южна земя. Най-малко 40 000 години " +
                "преди откриването от европейците в края на 18 век, Австралия е била обитавана от коренните австралийци, които принадлежали към повече от 250 езикови групи. Континентът" +
                "е открит от холандски изследователи през 1606 г. През 1770 г. източната му половина е обявена за владение на Великобритания. Първоначално на 7 февруари 1788 г. е " +
                "основана колонията Нов Южен Уелс, заселвана с каторжници. Постепенно населението нараства и през следващите десетилетия континентът е изследван напълно. Създадени са " +
                "още пет самоуправляващи се колонии. На 1 януари 1901 г. шестте колонии стават федерация и е създаден Австралийският съюз. Австралия поддържа стабилна либерална " +
                "демократична политическа система в Общността на нациите. Населението на страната днес е 22 милиона души, като около 60% е концентрирано в и около столицата Канбера " +
                "и големите градове Сидни, Мелбърн, Брисбейн, Пърт и Аделаида. Австралия е просперираща развита държава, тринадесетата по големина икономика в света. Страната се нарежда " +
                "на високо ниво в степента на човешко развитие, качеството на живот, здравеопазване, продължителност на живота, общественото образование, икономическата свобода и защитата " +
                "на гражданските свободи и политически права. Австралия е член на Организацията на обединените нации, Г-20, Общността на нациите, ANZUS, ОИСР, APEC, Форума на " +
                "тихоокеанските острови и на Световната търговска организация.");
    }

}
